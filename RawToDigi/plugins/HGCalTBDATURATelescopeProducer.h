//author: Thorben Quast
//date: 18th May 2018
//producer reading reconstructed DATURA telescope data and additing to the root file
//v1 (18th May 2018) also implements a preliminary straight line tracking to compute reference points on the sensors
//v2 (25th March 2018) implementing GBL

//definition of radiation lengths for GBL tracking
// X0 Si = 21.82/2.33 = 9.365 cm
#define X0_Si 93.65
// X0 air = 36.66/1.204E-3 = 303.9 m
#define X0_Air 303900.0
// X0 Kapton =  40.56 / 1.42 = 28.56 cm
#define X0_Kapton  285.6

#define MIMOSA26_MATERIAL_BUDGET 50e-3 / X0_Si + 50e-3 / X0_Kapton
#define MIMOSA26_CLUSTER_RESOLUTION 0.0184 / 10 / sqrt(12.)


#ifndef HGCALTBDATURATELESCOPEPRODUCER_H
#define HGCALTBDATURATELESCOPEPRODUCER_H

#include "FWCore/Framework/interface/EDProducer.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/EventSetup.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "FWCore/Framework/interface/ESHandle.h"


#include <iostream>
#include "HGCal/DataFormats/interface/HGCalTBRunData.h"
#include "HGCal/DataFormats/interface/HGCalTBDATURATelescopeData.h"

//for sttraight line tracking
#include "HGCal/Reco/interface/PositionResolutionHelpers.h"
#include "HGCal/Reco/interface/Tracks.h"

#include "CommonTools/UtilAlgos/interface/TFileService.h"
#include "TFile.h"
#include "TTree.h"
#include "TH2F.h"

class HGCalTBDATURATelescopeProducer : public edm::EDProducer{

  public:
    HGCalTBDATURATelescopeProducer(const edm::ParameterSet&);
    virtual void produce(edm::Event&, const edm::EventSetup&);
    virtual void beginJob();
    virtual void endJob();
 
  private:
    edm::EDGetTokenT<RunData> RunDataToken; 
    
    std::string outputCollectionName;

    std::string inputFile;
    TFile* rootFile;
    TTree* tree;
    int SkipFirstNEvents;

     
    TBranch                   *b_event;   
    TBranch                   *b_Ntracks;  
    TBranch                   *b_chi2;  
    std::map<int, TBranch*> b_clusterX;
    std::map<int, TBranch*> b_clusterY;
    std::map<int, TBranch*> b_clusterZ;
    std::map<int, TBranch*> b_absorber;


    int tree_eventID;
    int tree_Ntracks;
    std::vector<double> *tree_track_chi2;
    std::map<int, std::vector<double>* > tree_clusterX;       //in mm
    std::map<int, std::vector<double>* > tree_clusterY;       //in mm
    std::map<int, std::vector<double>* > tree_clusterZ;       //in mm
    std::map<int, std::vector<double>* > tree_absorber;       //in X0

    //for computation of reference positions at each layer
    unsigned short NHGCalLayers;
    std::string m_layerPositionFile;
    std::map<int, std::pair<double, double> > layerPositions;
    
    bool m_considerAirInGBLTracking;
    double m_beamEnergyForGBLTracking;

    std::string m_PIStagePositionFile;
    std::map<int, std::pair<float, float> > PIStagePositions;

    //residual histograms
    std::map<int, TH2F*> DATURA_residuals_full_track;
    std::map<int, TH2F*> DATURA_residuals_triplet_track;
    std::map<int, TH2F*> DATURA_residuals_GBL_track;
};
#endif
