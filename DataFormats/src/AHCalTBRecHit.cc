#include "HGCal/DataFormats/interface/AHCalTBRecHit.h"
#include "DataFormats/CaloRecHit/interface/CaloRecHit.h"

#include <iostream>

AHCalTBRecHit::AHCalTBRecHit() : CaloRecHit()
{
}


AHCalTBRecHit::AHCalTBRecHit(const DetId& id, Float16_t energy, Float16_t time, uint32_t flags, uint32_t aux) :
  CaloRecHit(id, energy, time, flags)
{
     m_AHCalRow = -999;
     m_AHCalCol = -999;
     m_AHCalLayer = -999;
     m_AHCalHitX = 0.;
     m_AHCalHitY = 0.;
     m_AHCalHitZ = 0.;
}

std::ostream& operator<<(std::ostream& s, const AHCalTBRecHit& hit)
{
  return s << hit.id() << ": " << hit.energy() << " GeV, ";

}
